package AI;

public class PadAction {
	private int wait, buttonKey;
	private boolean executed;
	public PadAction(int wait, int buttonKey) {
		setWait(wait);
		setButtonKey(buttonKey);
		setExecuted(false);
	}
	
	public void setWait(int wait) throws RuntimeException {
		if (wait < 0)
			throw new RuntimeException("negitive wait not allowed");
		this.wait = wait;
	}
	public int getWait() { return this.wait; }
	
	public void setButtonKey(int buttonKey) { this.buttonKey = buttonKey; }
	public int getButtonKey() { return this.buttonKey; }
	
	public void setExecuted(boolean executed) { this.executed = executed; }
	public boolean executed() { return this.executed; } 
}
