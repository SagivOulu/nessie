package AI;

import javax.swing.UIManager;

import com.grapeshot.halfnes.JInputHelper;
import com.grapeshot.halfnes.ui.SwingUI;

public class AIThreader implements Runnable {
	private Thread thread;
	private AI_IO ai_io;
	private boolean isDone; 
	
	public AIThreader(AI_IO ai_io) {
		this.ai_io = ai_io;
		this.isDone = false;
	}
	
	public AI_IO getAI() { return this.ai_io; }
	
	public boolean isDone() { return this.isDone; }
	
	public void start() {
		if (this.thread == null) {
			this.isDone = false;
			this.thread = new Thread(this);
			this.thread.start();
		}
	}
	
	@Override
	public void run() {
		JInputHelper.setupJInput();
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("Could not set system look and feel. Meh.");
        }
        new SwingUI(new String[]{"ROMs\\Super Mario Bros. (Japan, USA).nes"}, this.ai_io);
        this.isDone = true;
	}
}
