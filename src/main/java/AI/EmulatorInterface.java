package AI;

public interface EmulatorInterface {
	public static enum Button {
		Select(32),
		Start(10),
		A(88),
		B(90),
		Left(37),
		Right(39),
		Down(40);
		private int key;
		Button(int key) { this.key = key; }
		public int getKey() { return this.key; }
	}
	
	public static final int[] buttonId = new int[]{
			Button.Select.getKey(), Button.Start.getKey(), 
			Button.A.getKey(), Button.B.getKey(), 
			Button.Left.getKey(), Button.Right.getKey(), Button.Down.getKey()};
	public static final String[] buttonName = new String[]{"Select", "Start", "A", "B", "Left", "Right", "Down"};
	
	/**
	 * implements the main AI thinking algorithm 
	 * @return a boolean array of the buttons states
	 */
	public void think();
	/**
	 * initiates the game (wait X seconds for the emu to load, select the correct menu item...)
	 * this function is called each frame
	 * @return a boolean array of the buttons states
	 */
	public void init();
	/**
	 * ends the game (prints results ect...)
	 * this function is called each frame
	 * @return a boolean array of the buttons states
	 */
	public void end();
	
	public boolean[] getButtons();
	
	public boolean getButton(int index);
	
	public void setButtons(boolean[] buttons);
	
	public static int getButtonId(int index) { return buttonId[index]; }
	
	public static String getButtonName(int index) { return buttonName[index]; }
	
	public static int getNumOfButtons() { return 7; }
	
	public void setInitStartTime(long time);
	public long getInitStartTime();
	
	public void setThinkStartTime(long time);
	public long getThinkStartTime();
	
	public void setEndStartTime(long time);
	public long getEndStartTime();
	
	/**
	 * 
	 * @return a boolean value representing whether or not the inti is done
	 */
	public boolean isInitDone();
	/**
	 * 
	 * @return a boolean value representing whether or not think is done
	 */
	public boolean isThinkDone();
	/**
	 * 
	 * @return a boolean value representing whether or not the end is done
	 */
	public boolean isEndDone();
	
	/**
	 * copies the emu's RAM into the class 
	 * @param RAM an integer array of size 2K (2048)
	 */
	public void setRAM(int[] RAM);
	
	/**
	 * returns a bitmap overlay to print on top of the game </n>
	 * a negative value of a pixel is not printed, all else overrides the game print with the output pixel
	 * @return a 240 row and 256 column integer array representing the screen pixels to overlay; 
	 */
	public int[][] getPPUOverlay();
}
