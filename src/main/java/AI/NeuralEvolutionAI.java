package AI;

import java.util.Vector;

import NN.NeuralNetwork;

public class NeuralEvolutionAI extends AI_IO {
	private NeuralNetwork nn;
	private double aiFitness;
	private int maxXValue = 0;
	private static int numOfReadings = 15;//6;//54
	private int[] prevRAM;
	private double[] recurciveMemoryNodes = new double[10];
	
	public void setNeuralNetwork(NeuralNetwork nn) { this.nn = nn; }
	public NeuralNetwork getNeuralNetwork() { return this.nn; }
	
	public void setAIScore(double aiFitness) { this.aiFitness = aiFitness; }
	public double getAIFitness() { return this.aiFitness; }
	
	private void setPrevRAM(int[] prevRAM) { this.prevRAM = prevRAM; }
	private int[] getPrevRAM() { return this.prevRAM; }
	
	@Override
	public void AIThink() {
		setButtons(null);
		double[] inputs = getGameInputs();
		double[] outputs = getNeuralNetwork().getOutputs(inputs);
		boolean[] booleanOutputs = toBoolean(outputs, 0.5);
		//setButtons(outputs);
		//setButtons(new boolean[]{false, false, outputs[0], false, false, outputs[1], false});
		setButtons(new boolean[]{false, false, booleanOutputs[0], false, booleanOutputs[1], booleanOutputs[2], false});
		
		if (MarioIO.getPlayerPosition(getRAM())[0] > maxXValue)
			maxXValue = MarioIO.getPlayerPosition(getRAM())[0];
		
		setPrevRAM(getRAM().clone());
		
		for (int i = 3; i < this.recurciveMemoryNodes.length - 3; i++)
			this.recurciveMemoryNodes[i - 3] = outputs[i];
	}
	
	private void hardCodedAI() {
		setButtons(new boolean[]{false, false, false, false, false, true, false});
		
		//if front of Mario there is a block
		for (int i = 0; i < 8; i++)
			for (int j = 0; j < 30; j++)
				if (MarioIO.getTile(getRAM(), i, j) == 1) getButtons()[2] = true;
		
		//Mario is in the air OR hole in front of Mario
		if (MarioIO.getPlayerFloatState(getRAM()) == 1 || MarioIO.getTile(getRAM(), 20, 57) == 0) getButtons()[2] = true;
		if (!getButtons()[2]) {
			int[][] sprites = MarioIO.getSprites(getRAM());
			for (int i = 0; i < 5; i++)
				if (sprites != null && sprites[i] != null && sprites[i][0] > MarioIO.getPlayerPosition(getRAM())[0] && sprites[i][0] - 30 < MarioIO.getPlayerPosition(getRAM())[0])
					getButtons()[2] = true;
		}
		if (MarioIO.getPlayerFloatState(getRAM()) == 2) getButtons()[2] = false;
	}
	
	public static int getNumOfReadings() { return NeuralEvolutionAI.numOfReadings; }
	
	private double[] getGameInputs() {
		Vector<Double> readings = new Vector<Double>();
		
		readings.add(new Double(1)); //constant reading
		
		int[] marioPos = MarioIO.getPlayerPosition(getRAM());
		
		
		//front probe is an int that shows if there is an enemy/block in the area in front mario
		int frontProbe = 0;
		for (int i = 0; i < 8; i++)
			for (int j = 0; j < 30; j++)
				if (MarioIO.getTile(getRAM(), i, j) == 1) frontProbe = 1;
		int[][] sprites = MarioIO.getSprites(getRAM());
		for (int i = 0; i < 5 && frontProbe != 1; i++)
			if (sprites[i] != null)
				if (sprites[i][0] - marioPos[0] >= 0 && sprites[i][0] - marioPos[0] < 40)
					frontProbe = 1;
		readings.add(new Double(frontProbe)); //front probe reading
		
		/*
		//10 wide and 8 tall 8X8 sensor blocks (if there is a wall/floor/enemy in the block the value will be 1. else it will be 0)
		//System.out.println("************Map Start*************");
		for (int j = -2; j <= 4; j++) {
			for (int i = -3; i <= 3; i++) {
				if (MarioIO.isBlockEmpty(this.getRAM(), i, j, 16, 16)) {
					readings.add(0.0);
					//System.out.print(' ');
				}
				else {
					readings.add(1.0);
					//System.out.print('*');
				}
			}
			//System.out.println("");
		}
		//System.out.println("");
		*/
		
		
		readings.add(new Double(MarioIO.getPlayerFloatState(getRAM()))); //player float state reading
		
		/*
		//player previous float state reading
		if (getPrevRAM() != null) readings.add(new Double(MarioIO.getPlayerFloatState(getPrevRAM())));
		else readings.add(new Double(0));*/
		
		//game time normalized to 0, 1
		readings.add((double)MarioIO.getGameTime(getRAM()) / 400);
		
		//mario x pos / 256
		readings.add( (double)MarioIO.getPlayerPosition(getRAM())[0] / 200 );
		
		//add the memory output nodes from the previous propagation to the inputs of current propagation
		for (int i = 0; i < this.recurciveMemoryNodes.length; i++)
			readings.add(recurciveMemoryNodes[i]);
		
		//Transfer values from vector to array
		double[] readingsArray = new double[readings.size()];
		for (int i = 0; i < readingsArray.length; i++) readingsArray[i] = readings.get(i);
		return readingsArray;
	}
	
	@Override
	public boolean isInitDone() { return initDone; }

	@Override
	public boolean isThinkDone() {
		thinkingDone = (MarioIO.getGameTime(getRAM()) < 350 || MarioIO.getLivesLeft(getRAM()) != 2);
		return thinkingDone;
	}

	@Override
	public boolean isEndDone() { return endDone; }

	@Override
	public void init() { setAIScore(-1); }
	
	@Override
	public void gameEnd() {
		setAIScore(maxXValue); //set the score as the max Mario x location
		endDone = true;
	}

	@Override
	public void setButtons(boolean[] buttons) {
		super.setButtons(buttons);
	}
	
	private boolean[] toBoolean(double[] array, double threshold) {
		if (array == null) return null;
		boolean[] out = new boolean[array.length];
		for (int i = 0; i < out.length; i++)
			out[i] = array[i] > threshold;
		return out;
	}
}
