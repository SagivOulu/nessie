package AI;

import java.util.Random;
import java.util.Vector;

import javax.swing.UIManager;

import org.apache.tools.ant.taskdefs.Sleep;

import com.grapeshot.halfnes.JInputHelper;
import com.grapeshot.halfnes.ui.SwingUI;

import NN.*;
import evo.*;

public class Entry {
	static int generationCounter = 0;
	static int genotypesCheckedCounter = 0;
	
	static int populationSize = 20;
	static double crossoverRate = 0.2;
	static double mutationRate = 0.01;
	static Random random = new Random(2);
	
	static NeuralNetwork nn;
	static Pool pool = new Pool();
	
	public static void main(String[] args) {
		nn = new NeuralNetwork(NeuralEvolutionAI.getNumOfReadings(), 13, 2, 2);
		int genotypeLength = nn.getFlattenedWeightsLength();
		while (true)
		{
			System.out.println("running generation " + generationCounter);
			cycle(
					pool,
					genotypeLength,
					crossoverRate,
					mutationRate,
					populationSize);
			System.out.println("best fitness = " + round(pool.getBest().getFitness(), 2));
			generationCounter++;
		}
	}
	
	private static void cycle(Pool pool, int genotypeLength, double crossoverRate, double mutationRate, int poolSize)
    {
        //fill pool if empty with random genotypes
        if (pool.size() == 0) {
            for (int i = 0; i < poolSize; i++) {
                Genotype g = new Genotype(genotypeLength, pool.getRandom());
                fitness(g);
                pool.add(g);
            }
        }
        else
        {
            //create new pool for next generation
            Pool newPool = new Pool(pool.getRandom());
            pool.sortHighToLow();
            for (int i = 0; i < poolSize; i++)
            {
                Genotype[] parents = new Genotype[2];
                for (int j = 0; j < parents.length; j++)
                	parents[j] = pool.rouletteSelection();
                
                Genotype child;
                child = Genotype.continuousCrossover(parents[0], parents[1], crossoverRate);

                child.mutate(mutationRate);
                fitness(child);
                newPool.add(child);
            }
            
            pool.add(newPool);
            pool.leave(poolSize);
        }
    }
	
	private static void threadedFitness(Genotype[] genotypes) {
		AIThreader[] aiThreaders = new AIThreader[genotypes.length];
		for (int i = 0; i < genotypes.length; i++) {
			NeuralNetwork nn = (NeuralNetwork) Entry.nn.clone();
			nn.setWeights(genotypes[i].get());
			aiThreaders[i] = new AIThreader(new NeuralEvolutionAI());
			((NeuralEvolutionAI)aiThreaders[i].getAI()).setNeuralNetwork(nn);
			aiThreaders[i].start();
		}
		boolean allDone;
		
		do {
			System.out.println("****phenotype states****");
			allDone = true;
			for (int i = 0; i < aiThreaders.length; i++)
				if (!aiThreaders[i].isDone()) {
					allDone = false;
					System.out.println("--phenotype " + i + " not done");
				}
				else {
					System.out.println("--phenotype " + i + " not done");
				}
		} while(!allDone);
		
		double fitness;
		for (int i = 0; i < genotypes.length; i++) {
			fitness = ((NeuralEvolutionAI)aiThreaders[i].getAI()).getAIFitness();
			genotypes[i].setFitness(fitness);
			genotypesCheckedCounter++;
		}
	}
	
	private static void fitness(Genotype g) {
		nn.setWeights(g.get());
		NeuralEvolutionAI neuralEvolutionAI = new NeuralEvolutionAI();
		neuralEvolutionAI.setNeuralNetwork(nn);
		
		System.out.print("--testing genotype " + (genotypesCheckedCounter - (generationCounter * populationSize)) + " out of " + populationSize + " ...");
		JInputHelper.setupJInput();
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.err.println("Could not set system look and feel. Meh.");
        }
        new SwingUI(new String[]{"ROMs\\Super Mario Bros. (Japan, USA).nes"}, neuralEvolutionAI);
        double fitness = neuralEvolutionAI.getAIFitness();
        System.out.println(" fitness = " + fitness);
        g.setFitness(fitness);
        genotypesCheckedCounter++;
	}
	
	private static double round(double num, int after) { return (int)(num * Math.pow(10, after)) / Math.pow(10, after); }
}