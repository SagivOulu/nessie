package AI;

/**
 * this is an interface class that allows you to extract information from a RAM snapshot of a "Super Mario Bros" game. </br>
 * 
 * credit to SethBling and his marIO project. it was my inspiration for this project,
	and most of the memory reading specifics came from his Lua MarI/O project
 * @author sagiv
 *
 */
public class MarioIO {
	
	public static boolean isBlockEmpty(int[] RAM, int bx, int by, int blockWidth, int blockHeight) {
		//check for each sprite is it in the block
		int[][] sprites = MarioIO.getSprites(RAM);
		int[] marioPos = MarioIO.getPlayerPosition(RAM);
		for (int i = 0; i < sprites.length; i++)
			if (sprites[i] != null && 
			(sprites[i][0] - marioPos[0]) / blockWidth == bx && (sprites[i][1] - marioPos[1]) / blockHeight == by)
				return false;
		
		//for each pixel in the block
		for (int i = 0; i < blockWidth; i++)
			for (int j = 0; j < blockHeight; j++)
				if (MarioIO.getTile(RAM, bx * blockWidth + i, by * blockHeight + j) != 0)
					return false;
		
		return true;
	}
	
	/**
	 * returns the absolute {X, Y} location of Mario (the most left point on the map is x=0 and from there the value increases) 
	 * @param RAM a capture of the RAM from which to pull the data
	 * @return an integer array of size 2 containing Mario {X, Y} (in that order) absolute location
	 */
	public static int[] getPlayerPosition(int[] RAM) {
		return new int[]{
				RAM[0x6D] * 0x100 + RAM[0x86],
				RAM[0x03B8] + 0X10};
	}
	
	/**
	 * get tile in dx and dy distance of Mario.
	 * @param RAM a capture of the RAM from which to pull the data
	 * @param dx x distance from Mario
	 * @param dy y distance from Mario
	 * @return 1 if a tile is there, and 0 if not
	 */
	public static int getTile(int[] RAM, int dx, int dy) {
		int x = getPlayerPosition(RAM)[0] + dx + 8;
		int y = getPlayerPosition(RAM)[1] + dy - 16;
		int page = getPage(RAM);
		
		int subX = (int)Math.floor((x % 256) / 16);
		int subY = (int)Math.floor((y - 32) / 16);
		
		int addr = 0x500 + page * 13 * 16 + subY * 16 + subX;
		
		if (subY >= 13 || subY < 0)
			return 0;
		if (RAM[addr] != 0)
			return 1;
		return 0;
	}

	/**
	 * 
	 * @param pos the Row, Column Mario relative pos
	 * @return X, Y Screen Pixel
	 */
	public static int[] PPUPosToMarioPos(int[] pos, int[] RAM) {
		//overlay[i][j] = (MarioIO.getTile(getRAM(), j - 125, (i + 15) - MarioIO.getPlayerPosition(getRAM())[1]) == 1? 15: -1);
		int[] out = new int[]{pos[1] - 125,pos[0] + 15 - MarioIO.getPlayerPosition(RAM)[1]};
		return out;
	}
	
	public static int getPage(int[] RAM) { return (int)Math.floor(getPlayerPosition(RAM)[0] / 256) % 2; }

	/**
	 * the NES can store only 5 sprites at a time, so the returned array size is always of size 5;
	 * @param RAM a capture of the RAM from which to pull the data
	 * @return an integer array of size 5 containing integer arrays of size 2 containing the {X, Y} (in this order) position of the 5 sprites. </br>
	 * 			the the arrays of the unloaded sprites will be of a NULL value
	 */
	public static int[][] getSprites(int[] RAM) {
		int[][] out = new int[5][2];
		for (int i = 0; i < 4; i++) {
			int enemy = RAM[0xF + i];
			if (enemy == 0)
				out[i] = null;
			else {
				out[i][0] = RAM[0x6E + i] * 0x100 + RAM[0x87 + i];
				out[i][1] = RAM[0xCF + i] + 24;
			}
		}
		return out;
	}

	/**
	 * returns the players state represented by an integer
	 * 0 = Standing on solid/else
	 * 1 = Air born by jumping
	 * 2 = Air born by walking of a ledge
	 * 3 = Sliding down flag pole
	 * @param RAM a capture of the RAM from which to pull the data
	 * @return an integer representing the players (Mario) floating state
	 */
	public static int getPlayerFloatState(int[] RAM) {
		if (RAM == null) throw new RuntimeException("null parameter given to getPlayerFloatState(int[] RAM)");
		return RAM[0x001D];
	}
	
	/**
	 * returns the game timer value. the timer starts at 400, and decreases until reaching 0
	 * @param RAM RAM a capture of the RAM from which to pull the data
	 * @return the game timer value
	 */
	public static int getGameTime(int[] RAM) {
		/*
		 * the game time is stored in 3 bytes, starting at 0x07f8.
		 * the values in the 3 bytes are the digits of the game time, from the 100s digit downward.
		 * if the game time is 329, the RAM will look like this:
		 * [0x07f8] = 3
		 * [0x07f9] = 2
		 * [0x07fa] = 9
		 */
		int timerAddress = 0x07f8;
		int numOfDigits = 3;
		int time = 0;
		for (int i = numOfDigits - 1; i >= 0; i--) {
			time += RAM[timerAddress++] * Math.pow(10, i);
		}
		return time;
	}
	
	/**
	 * returns the number of lives count.
	 * if mario has not died, the value will be 2. if mario died once the value will be 1... 
	 * @param RAM
	 * @return
	 */
	public static int getLivesLeft(int[] RAM) { return RAM[0x075a]; }
	
	public static void printConsoleMap(int[] RAM) {
		int columns = 50, columnMulti = 5;
		int rows = 50, rowMulti = 5;
		String row = "   "; System.out.println("-------------------Start-------------------");
		for (int i = 0; i < 50; i++) {
			row += (i - (rows / 2)) * rowMulti;
			row += ((i - (rows / 2)) * rowMulti < 10? "***": ((i - (rows / 2)) * rowMulti < 100? "**": "*"));
		}
		System.out.println(row);
		for (int i = 0; i < columns; i++) {
			row = (i - (rows / 2)) * rowMulti + 
					((i - (rows / 2)) * rowMulti < 10? ":  ": ((i - (rows / 2)) * rowMulti < 100? ": ": ":"));
			for (int j = 0; j < rows; j++) {
				int x = (j - (columns / 2)) * columnMulti;
				int y = (i - (rows / 2)) * rowMulti;
				if (x == 0 && y == 0) row += 9;
				else row += (MarioIO.getTile(RAM, x, y) == 1? "[]": "  ");
				row += " ";
			}
			System.out.println(row);
		}
	}
	public static void printSpriteData(int[] RAM) {
		int[][] sprites = MarioIO.getSprites(RAM);
		String printS = "<";
		for (int i = 0; i < sprites.length; i++) {
			printS += (sprites[i] == null? "NaN": (sprites[i][0] + ":" + sprites[i][1]));
			if (i < sprites.length - 1) printS += ",";
		}
		printS += ">";
		System.out.println(printS);
	}
	public static void printMarioPos(int[] RAM) {
		System.out.println("mario pos: " + 
				MarioIO.getPlayerPosition(RAM)[0] + ", " + 
				MarioIO.getPlayerPosition(RAM)[1]);
	}
}
