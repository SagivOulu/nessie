package AI;

public abstract class AI_IO {
	private long initStartTime = -1, thinkStartTime = -1, endStartTime = -1;
	public boolean initDone = false, thinkingDone = false, endDone = true;
	private int[] RAM = new int[2048];
	private boolean[] buttons;
	
	public AI_IO() {
		setInitStartTime(-1);
		setThinkStartTime(-1);
		setEndStartTime(-1);
		init();
	}
	
	public void setInitStartTime(long initStartTime) {  this.initStartTime = initStartTime; }
	public long getInitStartTime() { return this.initStartTime; }
	
	public void setThinkStartTime(long thinkStartTime) { this.thinkStartTime = thinkStartTime; }
	public long getThinkStartTime() { return this.thinkStartTime; }
	
	public void setEndStartTime(long endStartTime) { this.endStartTime = endStartTime; }
	public long getEndStartTime() { return this.endStartTime; }
	
	public boolean[] getButtons() { return this.buttons; } 
	
	public boolean getButton(int index) {
		return (getButtons() != null && getButtons().length >= index && getButtons()[index]);
	}
	
	public void setButtons(boolean[] buttons) { this.buttons = buttons; }
	
	public void gameInit() {
		if (getInitStartTime() < 0) setInitStartTime(System.currentTimeMillis());
		
		if (System.currentTimeMillis() > getInitStartTime() + (2 * 1000)) {
			initDone = true;
			setButtons(new boolean[]{false, true, false, false, false, false, false});
			return;
		}
		setButtons(null);
	}
	
	public void gameThink() {
		if (getThinkStartTime() == -1)
			setThinkStartTime(System.currentTimeMillis());

		AIThink(); //this function sets the buttons array to the wanted values
	}
	
	public void setRAM(int[] RAM) { this.RAM = RAM.clone(); }
	
	public int[] getRAM() { return this.RAM; }
	
	public int[][] getPPUOverlay() {
		int[][] overlay = new int[240][256];
		/*for (int i = 0; i < overlay.length; i++)
			for (int j = 0; j < overlay[0].length; j++)
				overlay[i][j] = (j % 8 == 0 && i % 8 == 0? 15: -1);*/
		for (int i = 0; i < overlay.length; i++)
			for (int j = 0; j < overlay[0].length; j++) {
				//int val = MarioIO.getTile(getRAM(), MarioIO.PPUPosToMarioPos(new int[]{i, j}, getRAM())[0], MarioIO.PPUPosToMarioPos(new int[]{i, j}, getRAM())[1]);
				overlay[i][j] = -1;//(val == 1? 15: -1);
			}
		
		int row = 8 * 9, column = 8 * 7, clickedColor = 0x16, unclickedColor = 0;
		addMatrix(overlay,  row, column, replaceMatrix(A, 0, (getButton(2)? clickedColor: unclickedColor) ));
        column += 32;
        addMatrix(overlay,  row, column, replaceMatrix(B, 0, (getButton(3)? clickedColor: unclickedColor) ));
        column += 32;
        addMatrix(overlay,  row, column, replaceMatrix(Left, 0, (getButton(4)? clickedColor: unclickedColor) ));
        column += 32;
        addMatrix(overlay,  row, column, replaceMatrix(Right, 0, (getButton(5)? clickedColor: unclickedColor) ));
        column += 32;
        addMatrix(overlay,  row, column, replaceMatrix(Down, 0, (getButton(6)? clickedColor: unclickedColor) ));
        
		return overlay;
	}
	
	private void addMatrix(int[][] origin, int row, int column, int[][] addition) {
		for (int i = 0; i < addition.length; i++)
			for (int j = 0; j < addition[0].length; j++)
				if (i + row < origin.length && j + column < origin[0].length && addition[i][j] >= 0)
					origin[row + i][column + j] = addition[i][j];
	}
	
	private int[][] replaceMatrix(int[][] matrix, int original, int newVal) {
		int[][] copy = matrix.clone();
		for (int i = 0; i < copy.length; i++)
			for (int j = 0; j < copy[0].length; j++)
				if (copy[i][j] == original)
					copy[i][j] = newVal;
		return copy;
	}
	
	public abstract void init();
	public abstract void AIThink();
	public abstract void gameEnd();
	public abstract boolean isInitDone();
	public abstract boolean isThinkDone();
	public abstract boolean isEndDone();
	
	int[][] A = new int[][]{
		{ -1, -1, -1,  0,  0, -1, -1, -1}, 
		{ -1, -1,  0,  0,  0,  0, -1, -1},
		{ -1,  0,  0, -1, -1,  0,  0, -1},
		{ -1,  0,  0,  0,  0,  0,  0, -1},
		{ -1,  0,  0, -1, -1,  0,  0, -1},
		{ -1,  0,  0, -1, -1,  0,  0, -1},
		{ -1,  0,  0, -1, -1,  0,  0, -1},
		{ -1, -1, -1, -1, -1, -1, -1, -1}};
	int[][] B = new int[][]{
    	{ -1,  0,  0,  0,  0,  0, -1, -1}, 
    	{ -1,  0,  0, -1, -1,  0,  0, -1},
    	{ -1,  0,  0, -1, -1,  0,  0, -1},
    	{ -1,  0,  0,  0,  0,  0, -1, -1},
    	{ -1,  0,  0, -1, -1,  0,  0, -1},
    	{ -1,  0,  0, -1, -1,  0,  0, -1},
    	{ -1,  0,  0,  0,  0,  0, -1, -1},
    	{ -1, -1, -1, -1, -1, -1, -1, -1}};
    int[][] Blank = new int[][]{
        { -1, -1, -1, -1, -1, -1, -1, -1}, 
        { -1, -1, -1, -1, -1, -1, -1, -1},
        { -1, -1, -1, -1, -1, -1, -1, -1},
        { -1, -1, -1, -1, -1, -1, -1, -1},
        { -1, -1, -1, -1, -1, -1, -1, -1},
        { -1, -1, -1, -1, -1, -1, -1, -1},
        { -1, -1, -1, -1, -1, -1, -1, -1},
        { -1, -1, -1, -1, -1, -1, -1, -1}};
    int[][] Up = new int[][]{
        { -1, -1, -1,  0,  0, -1, -1, -1}, 
        { -1, -1,  0,  0,  0,  0, -1, -1},
        { -1,  0,  0,  0,  0,  0,  0, -1},
        { -1,  0, -1,  0,  0, -1,  0, -1},
        { -1, -1, -1,  0,  0, -1, -1, -1},
        { -1, -1, -1,  0,  0, -1, -1, -1},
        { -1, -1, -1,  0,  0, -1, -1, -1},
        { -1, -1, -1,  0,  0, -1, -1, -1}};
    int[][] Down = new int[][]{
        { -1, -1, -1,  0,  0, -1, -1, -1}, 
        { -1, -1, -1,  0,  0, -1, -1, -1},
        { -1, -1, -1,  0,  0, -1, -1, -1},
        { -1, -1, -1,  0,  0, -1, -1, -1},
        { -1,  0, -1,  0,  0, -1,  0, -1},
        { -1,  0,  0,  0,  0,  0,  0, -1},
        { -1, -1,  0,  0,  0,  0, -1, -1},
        { -1, -1, -1,  0,  0, -1, -1, -1}};
    int[][] Right = new int[][]{
        { -1, -1, -1, -1, -1, -1, -1, -1}, 
        { -1, -1, -1, -1,  0,  0, -1, -1},
        { -1, -1, -1, -1, -1,  0,  0, -1},
        {  0,  0,  0,  0,  0,  0,  0,  0},
        {  0,  0,  0,  0,  0,  0,  0,  0},
        { -1, -1, -1, -1, -1,  0,  0, -1},
        { -1, -1, -1, -1,  0,  0, -1, -1},
        { -1, -1, -1, -1, -1, -1, -1, -1}};
    int[][] Left = new int[][]{
        { -1, -1, -1, -1, -1, -1, -1, -1}, 
        { -1, -1,  0,  0, -1, -1, -1, -1},
        { -1,  0,  0, -1, -1, -1, -1, -1},
        {  0,  0,  0,  0,  0,  0,  0,  0},
        {  0,  0,  0,  0,  0,  0,  0,  0},
        { -1,  0,  0, -1, -1, -1, -1, -1},
        { -1, -1,  0,  0, -1, -1, -1, -1},
        { -1, -1, -1, -1, -1, -1, -1, -1}};

}