package evo;

import java.util.Random;
import java.util.Vector;

public class Pool {
    private Random random;

    private Vector<Genotype> genotypes;

    public Pool()
    {
        this.random = new Random();
        genotypes = new Vector<Genotype>();
    }

    public Pool(Random random)
    {
        this.random = random;
        genotypes = new Vector<Genotype>();
    }

    public Pool clone()
    {
        Pool pool = new Pool();
        pool.add(getAll());
        return pool;
    }

    public int size() { return this.genotypes.size(); }

    public void clear() { this.genotypes.clear(); }

    public void setRandom(Random random) { this.random = random; }

    public Random getRandom() { return this.random; }

    public void add(Genotype g)
    {
        this.genotypes.add(g);
    }

    public void add(Genotype[] g)
    {
        for (int i = 0; i < g.length; i++) add(g[i]);
    }

    public void add(Pool p)
    {
        if (p == null) throw new RuntimeException("unable to add null Pool to pool");
        add(p.getAll());
    }

    public void set(int index, Genotype g)
    {
        if (index >= size()) throw new IndexOutOfBoundsException("pool genotype index out of bounds: " + index);
        this.genotypes.set(index, g);
    }

    public Genotype get(int index)
    {
        if (index >= size()) throw new IndexOutOfBoundsException("pool genotype index out of bounds: " + index);
        return this.genotypes.get(index);
    }

    public Genotype getBest()
    {
        if (size() == 0) return null;
        int bestIndex = 0;
        for (int i = 1; i < size(); i++)
            if (get(i).getFitness() > get(bestIndex).getFitness())
                bestIndex = i;
        return get(bestIndex);
    }

    public void remove(int index)
    {
        if (index >= size()) throw new IndexOutOfBoundsException("pool genotype index out of bounds: " + index);
        this.genotypes.remove(index);
    }

    public Genotype[] getAll() {
    	Genotype[] genotypesArray = new Genotype[size()];
    	for (int i = 0; i < genotypesArray.length; i++) genotypesArray[i] = get(i);
    	return genotypesArray;
    }

    public void sortHighToLow()
    {
        if (!this.isSortedHighToLow())
        {
            Genotype temp;
            for (int i = 0; i < size(); i++)
                for (int j = 1; j < size(); j++)
                {
                    if (get(j - 1).getFitness() < get(j).getFitness())
                    {
                        temp = get(j);
                        set(j, get(j - 1));
                        set(j - 1, temp);
                    }
                }
        }
    }

    public boolean isSortedHighToLow()
    {
        for (int i = 0; i < size() - 1; i++)
            if (get(i).getFitness() < get(i + 1).getFitness())
                return false;
        return true;
    }

    public Genotype rouletteSelection()
    {
        float totalScore = 0;
        float runningScore = 0;
        for (Genotype g : genotypes) {
        	totalScore += (float)g.getFitness();
		}
        float rnd = (float)(random.nextDouble() * totalScore);

        for (Genotype g : getAll())
        {
            if (rnd >= runningScore && rnd <= runningScore + g.getFitness())
            {
                return g;
            }
            runningScore += (float)g.getFitness();
        }

        return null;
    }

    public Genotype rankSelection()
    {
        if (!isSortedHighToLow()) sortHighToLow();

        double probabilityConst = 0.4;
        float totalProbability = 0;
        float runningProbability = 0;

        for (int i = 0; i < size(); i++)
        {
            totalProbability += (float)getRankProbability(probabilityConst, i);
        }

        float rnd = (float)(random.nextDouble() * totalProbability);

        for (int i = 0; i < size(); i++)
        {
            if (rnd >= runningProbability && rnd <= runningProbability + getRankProbability(probabilityConst, i))
            {
                return get(i);
            }
            runningProbability += (float)getRankProbability(probabilityConst, i);
        }

        return null;
    }
    private double getRankProbability(double probabilityConst, int index)
    {
        return Math.pow(1 - probabilityConst, index) * probabilityConst;
    }

    public void leave(int ammount)
    {
        int minIndex;
        while (size() > ammount)
        {
            minIndex = 0;
            for (int i = 1; i < size(); i++)
            {
                if (get(i).getFitness() < get(minIndex).getFitness())
                    minIndex = i;
            }
            remove(minIndex);
        }
    }
}
