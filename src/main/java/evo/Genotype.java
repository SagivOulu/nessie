package evo;

import java.util.Random;
import java.util.Vector;

public class Genotype {
    private Random random;

    private Vector<Double> genes;
    private double fitness;
    
    /**
     * most basic constructor. create a genotype with random values for the genes between 0 and 1
     * @param length the length of the genotype
     */
    public Genotype(int length)
    {
    	
    	random = new Random();
        this.genes = new Vector<Double>();
        for (int i = 0; i < length; i++) genes.add(random.nextDouble());
        random = new Random();
    }
    
    /**
     * most basic constructor. create a genotype with random values for the genes between 0 and 1.
     * use this constructor to set the genotypes random object to a given instance in order to use a random seed
     * @param length length the length of the genotype
     * @param random sets the random object of the genotype to a given random instance
     */
    public Genotype(int length, Random random)
    {
    	random = new Random();
        this.random = random;
        this.genes = new Vector<Double>();
        for (int i = 0; i < length; i++) genes.add(random.nextDouble());
    }

    /**
     * returns the number of genes in the genotype
     * @return number of genes
     */
    public int size() { return this.genes.size(); }

    /**
     * set the random object used by the genotype to a given instance.
     * use this to make the genotype to use a random instance with preset seed
     * @param random
     */
    public void setRandom(Random random) { this.random = random; }

    /**
     * returns the value of a gene from the genotype
     * @param index the index of the gene
     * @return double value of the gene
     */
    public double get(int index)
    {
        if (index >= size()) throw new IndexOutOfBoundsException("invalid genotype gene index " + index);
        return this.genes.get(index);
    }

    /**
     * returns an array of the genes values
     * @return array of genes values
     */
    public double[] get() {
    	double[] genesArray = new double[size()];
    	for (int i = 0; i < genesArray.length; i++) genesArray[i] = get(i);
    	return genesArray;
    }
    
    /**
     * sets a gene to a new value
     * @param index the index of the gene
     * @param value the new value to set the gene to
     */
    public void set(int index, double value)
    {
        if (index >= size()) throw new IndexOutOfBoundsException("invalid genotype gene index " + index);
        this.genes.set(index, value);
    }

    /**
     * adds a new gene to the genotype
     * @param gene the gene to add
     */
    public void add(double gene) { this.genes.add(gene); }
    
    /**
     * sets the genes fitness
     * @param fitness the fitness value
     */
    public void setFitness(double fitness) { this.fitness = fitness; }

    /**
     * returns the genotypes fitness
     * @return the genotypes fitness
     */
    public double getFitness() { return this.fitness; }

    /**
     * mutates the genotypes genes at a given rate.
     * for example: if the genotype size is 10 and the mutation rate is 0.3, on average 3 genes will mutate.
     * @param mutationRate the rate of mutation
     * @throws RuntimeException
     */
    public void mutate(double mutationRate) throws RuntimeException
    {
        if (mutationRate < 0 || mutationRate > 1) throw new RuntimeException("invalid mutationRate. must be between 0 and 1 (including both)");
        for (int i = 0; i < size(); i++)
            if (random.nextDouble() < mutationRate)
                set(i, random.nextDouble());
    }

    /**
     * return a new genotype created by single point crossover between two parents.
     * single point crossover creates child be splicing together the two parents at a single point.
     * @param g1 parent
     * @param g2 parent
     * @return child genotype
     * @throws RuntimeException
     */
    public static Genotype singlePointCrossover(Genotype g1, Genotype g2) throws RuntimeException
    {
        if (g1.size() != g2.size()) throw new RuntimeException("invalid singlePointCrossover between genotypes of different sizes");
        Genotype g = new Genotype(0, g1.random);
        int point = g1.random.nextInt(g1.size());
        for (int i = 0; i < g1.size(); i++)
            if (i < point) g.add(g1.get(i));
            else g.add(g2.get(i));
        return g;
    }

    /**
     * return a new genotype created by continuous crossover between two parents.
     * continuous crossover create child by copying genes from one of the parents, and switching the parent from which the genes are copied at a given rate
     * @param g1 parent
     * @param g2 parent
     * @param crossoverRate the rate of switching between parents when copying the genes from parent to child
     * @return child
     * @throws RuntimeException
     */
    public static Genotype continuousCrossover(Genotype g1, Genotype g2, double crossoverRate) throws RuntimeException
    {
        if (g1.size() != g2.size()) throw new RuntimeException("invalid singlePointCrossover between genotypes of different sizes");
        if (crossoverRate < 0 || crossoverRate > 1)
            throw new IndexOutOfBoundsException("invalid crossover rate. must be between 0 inclusive and 1 inclusive");
        Genotype g = new Genotype(0, g1.random);
        int state = g1.random.nextInt(2) * 2 - 1;
        for (int i = 0; i < g1.size(); i++)
        {
            if (g1.random.nextDouble() < crossoverRate) state *= -1;
            if (state == 1) g.add(g1.get(i));
            else g.add(g2.get(i));
        }
        return g;
    }
}