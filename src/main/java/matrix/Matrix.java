package matrix;

public class Matrix {
	/**
	 * creates an array with [rows] rows and [columns] columns, and sets the values to [initValue]
	 * @param rows number of rows of the matrix
	 * @param columns number of columns of the matrix
	 * @param initValue the value to set all the values
	 * @return a double matrix of size[rows][columns]
	 */
	public static double[][] create(int rows, int columns, double initValue) {
		double[][] out = new double[rows][columns];
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
				out[i][j] = initValue;
		return out;
	}
	
	/**
	 * this method create a double matrix, and overlaps it with an addition matrix (the old value is replaced with the addition matrix value)
	 * @param rows int. the number of rows in the new matrix
	 * @param columns int. the number of columns in the new matrix
	 * @param rowIndex int. the row index were to overlap the addition matrix
	 * @param columnIndex int. the columns index were to overlap the addition matrix
	 * @param additionMatrix double[][]. the addition matrix
	 * @param initValue int. the value to set in the new cells
	 * @return double[][] matrix
	 * @throws IndexOutOfBoundsException
	 */
	public static double[][] create(int rows, int columns, int rowIndex, int columnIndex, double[][] additionMatrix, double initValue) throws IndexOutOfBoundsException {
		if (additionMatrix != null)
			if (rows < additionMatrix.length + rowIndex || columns < additionMatrix[0].length + columnIndex)
				throw new IndexOutOfBoundsException("invalid index or length of addition matrix");
		double[][] out = create(rows, columns, initValue);
		if (additionMatrix == null) return out;
		for (int i = 0; i < additionMatrix.length; i++)
			for (int j = 0; j < additionMatrix[0].length; j++)
				out[rowIndex + i][columnIndex + j] = additionMatrix[i][j];
		return out;
	}

	/**
	 * this method create a double matrix, and overlaps it with an addition matrix (the old value is replaced with the addition matrix value)
	 * @param rows int. the number of rows in the new matrix
	 * @param columns int. the number of columns in the new matrix
	 * @param additionMatrix double[][]. the addition matrix
	 * @param initValue int. the value to set in the new cells
	 * @return double[][] matrix
	 * @throws IndexOutOfBoundsException
	 */
	public static double[][] create(int rows, int columns, double[][] additionMatrix, double initValue) throws IndexOutOfBoundsException {
		return create(rows, columns, 0, 0, additionMatrix, initValue);
	}
	
	/**
	 * this method create a double matrix, and overlaps it with an addition matrix (the old value is replaced with the addition matrix value)
	 * @param rows int. the number of rows in the new matrix
	 * @param columns int. the number of columns in the new matrix
	 * @param additionMatrix double[][]. the addition matrix
	 * @return double[][] matrix
	 * @throws IndexOutOfBoundsException
	 */
	public static double[][] create(int rows, int columns, double[][] additionMatrix) throws IndexOutOfBoundsException {
		return create(rows, columns, 0, 0, additionMatrix, 0);
	}
	
	/**
	 * creates an array with [rows] rows and [columns] columns, and sets the values to 0
	 * @param rows number of rows of the matrix
	 * @param columns number of columns of the matrix
	 * @return a double matrix of size[rows][columns]
	 */
	public static double[][] create(int rows, int columns) {
		double[][] out = new double[rows][columns];
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
				out[i][j] = 0;
		return out;
	}

	/**
	 * add up two matrices.
	 * @param a first double matrix
	 * @param b second double matrix
	 * @return a + b double matrix
	 */
	public static double[][] add(double[][] a, double[][] b) throws RuntimeException{
		if (a.length != b.length && a[0].length != b[0].length)
			throw new RuntimeException("invalid matrix addition. unable to add two matrices of different sizes");
		if (a == null || b == null)
			return null;
		double[][] out = new double[a.length][b.length];
		for (int i = 0; i < out.length; i++)
			for (int j = 0; j < a[0].length; j++)
				out[i][j] = a[i][j] + b[i][j];
		return out;
	}
	
	/**
	 * multiplies a and b matrices
	 * @param a a double matrix
	 * @param b a double matrix
	 * @return a double matrix
	 */
	public static double[][] multiply(double[][] a, double[][] b) throws RuntimeException{
		if (a[0].length != b.length)
			throw new RuntimeException("invalid matrix multiplication on invalid matrix sizes");
		double[][] out = new double[a.length][b[0].length];
		for (int i = 0; i < out.length; i++)
			for (int j = 0; j < out[0].length; j++) {
				out[i][j] = 0;
				for (int k = 0; k < a[0].length; k++)
					out[i][j] += a[i][k] * b[k][j];
			}
		return out;
	}

	/**
	 * multiplies a matrix with a scalar
	 * @param scalar a double
	 * @param matrix a double matrix
	 * @return the matrix result of scalar * matrix
	 */
	public static double[][] multiply(double scalar, double[][] matrix) {
		if (matrix == null)
			return null;
		double[][] out = new double[matrix.length][matrix[0].length];
		for (int i = 0; i < matrix.length; i++)
			for (int j = 0; j < matrix[0].length; j++)
				out[i][j] = scalar * matrix[i][j];
		return out;
	}

	/**
	 * extends the size of a matrix by a given length(rows and columns) and sets the new values to [initValue]
	 * @param matrix a double matrix
	 * @param rowExtend the number of rows to extend the original matrix
	 * @param columnExtend the number of columns to extend the original matrix
	 * @param initValue a double value to set the new cells
	 * @return the extended double matrix
	 */
	public static double[][] extend(double[][] matrix, int rowExtend, int columnExtend, double initValue) {
		double[][] out = new double[matrix.length + rowExtend][matrix[0].length + columnExtend];
		for (int i = 0; i < out.length; i++)
			for (int j = 0; j < out[0].length; j++)
				out[i][j] = (i < matrix.length && j < matrix[0].length? matrix[i][j]: initValue);
		return out;
	}
	
	/**
	 * Flattens a two dimensional matrix into a one dimensional array
	 * @param matrix a two dimensional double matrix
	 * @return a double array
	 */
	public static double[] flatten(double[][] matrix) {
		double[] out = new double[matrix.length * matrix[0].length];
		for (int i = 0; i < matrix.length; i++)
			for (int j = 0; j < matrix[0].length; j++)
				out[i * matrix[0].length + j] = matrix[i][j];
		return out;
	}

	/**
	 * unflattens an array to a two dimensional array from a given index
	 * @param arr a one dimensional double array
	 * @param int. the index were the unflattening begins
	 * @param rows int. the number of rows in the unflattened matrix
	 * @param columns int. the number of columns in the unflattened matrix
	 * @return a double matrix
	 * @throws IndexOutOfBoundsException
	 */
	public static double[][] unflatten(double[] arr, int index, int rows, int columns) throws IndexOutOfBoundsException {
		if (arr == null)
			throw new NullPointerException("unflatten method given null array");
		if (index + rows * columns > arr.length)
			throw new IndexOutOfBoundsException("unflatten index or unflatten area is out of bounds");
		
		double[][] out = new double[rows][columns];
		for (int i = 0; i < rows; i++)
			for (int j = 0; j < columns; j++)
				out[i][j] = arr[index + i * columns + j];
		return out;
	}
	
	/**
	 * concates the flattened matrices into one long array
	 * @param the matrices to concate
	 * @return double[]. the concatenated array
	 */
	public static double[] concatFlattenMatrices(double[][]...matrices) {
		int length = 0, i, j, index = 0;
		for (i = 0; i < matrices.length; i++)
			length += matrices[i].length * matrices[i][0].length;
		double[] out = new double[length];
		double[] flattenMatrix = null;
		for (i = 0; i < matrices.length; i++) {
			flattenMatrix = flatten(matrices[i]);
			for (j = 0; j < flattenMatrix.length; j++)
				out[index + j] = flattenMatrix[j];
			index += j;
		}
		return out;
	}
	
	/**
	 * prints the matrix values to the console
	 * @param a a double matrix
	 */
	public static void printMartix(double[][] a) {
		for (int i = 0; i < a.length; i++) {
			for (int j = 0; j < a[0].length; j++)
				System.out.print(a[i][j] + (j == a[0].length - 1? "": ", "));
			System.out.println("");
		}
	}
}