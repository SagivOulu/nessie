package NN;

import matrix.*;

public class NeuralNetwork implements Cloneable {
	private int numOfInputs, numOfOutputs, numOfHiddenLayers, sizeOfHiddenLayer;
	private double[][][] weights;
	
	public NeuralNetwork(int numOfInputs, int numOfOutputs, int numOfHiddenLayers, int sizeOfHiddenLayer) {
		this.numOfInputs = numOfInputs;
		this.numOfOutputs = numOfOutputs;
		this.numOfHiddenLayers = numOfHiddenLayers;
		this.sizeOfHiddenLayer = sizeOfHiddenLayer;
		
		this.weights = new double[getNumOfHiddenLayers() + 1][][];
		getWeights()[0] = new double[getNumOfInputs() + 1][getSizeOfHiddenLayer()]; //adds weights between input layer and first hidden layer
		int i;
		for (i = 1; i < getNumOfHiddenLayers(); i++)
			getWeights()[i] = new double[getSizeOfHiddenLayer() + 1][getSizeOfHiddenLayer()]; //adds weights between the hidden layers
		getWeights()[i] = new double[getSizeOfHiddenLayer() + 1][getNumOfOutputs()]; //adds weights between the last hidden layer and the output layer
	}
	
	@Override
	public Object clone() {
		try {
			return super.clone();
		} catch (CloneNotSupportedException e) {
			throw new RuntimeException(e);
		}
	}
	
	public int getNumOfInputs() { return this.numOfInputs; }
	public int getNumOfOutputs() { return this.numOfOutputs; }
	public int getNumOfHiddenLayers() { return this.numOfHiddenLayers; }
	public int getSizeOfHiddenLayer() { return this.sizeOfHiddenLayer; }
	
	public void setWeights(double[] weights) throws IndexOutOfBoundsException {
		int length = 0;
		for (int i = 0 ; i < getWeights().length; i++)
			length += getWeights()[i].length * getWeights()[i][0].length;
		if (weights.length != length)
			throw new IndexOutOfBoundsException("invalid setWeights array length");
		int index = 0;
		for (int i = 0; i < getWeights().length; i++) {
			getWeights()[i] = Matrix.unflatten(weights, index, getWeights()[i].length, getWeights()[i][0].length);
			index += getWeights()[i].length * getWeights()[i][0].length;
		}
	}
	
	public double[][][] getWeights() { return this.weights; }
	
	public double[] getFlattenedWeights() { return Matrix.concatFlattenMatrices(getWeights()); }
	
	public int getFlattenedWeightsLength() {
		int length = 0;
		for (int i = 0; i < getWeights().length; i++)
			length += getWeights()[i].length * getWeights()[i][0].length;
		return length;
	}
	
	public double[] getOutputs(double[] inputs) {
		//convert the inputs from an array to a matrix in order to allow multiplying it with other matrices
		double[][] inputsMatrix = new double[][]{ inputs };
		inputsMatrix = Matrix.extend(inputsMatrix, 0, 1, -1); //adds a cell for the bias with a value of -1
		//multiply the input matrix with the first weight matrix to get the first hidden layer matrix (before activation and adding the bias)
		double[][] m = Matrix.multiply(inputsMatrix, getWeights()[0]);
		
		for (int i = 1; i < getWeights().length; i++) {
			m = activate(m);
			m = Matrix.extend(m, 0, 1, -1); //adds a cell for the bias with a value of -1
			m = Matrix.multiply(m, getWeights()[i]);
		}
		m = activate(m);
		return Matrix.flatten(m);
	}
	
	public boolean[] getBooleanOutputs(double[] inputs, double threshold) {
		double[] dOutputs = getOutputs(inputs);
		if (dOutputs == null)
			return null;
		boolean[] bOutputs = new boolean[dOutputs.length];
		for (int i = 0; i < dOutputs.length; i++)
			bOutputs[i] = dOutputs[i] > threshold;
		return bOutputs;
	}

	private double[][] activate(double[][] matrix) {
		double[][] out = matrix.clone();
		for (int i = 0; i < out.length; i++)
			for (int j = 0; j < out[0].length; j++)
				out[i][j] = 1 / (1 + Math.pow(Math.E, -1 * matrix[i][j]));
		return out;
	}
}
